package com.xeranas.gdx.sample.level;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.ScreenAdapter;
import com.xeranas.gdx.sample.scene2d.ActionPanel;
import com.xeranas.gdx.sample.utils.LevelController;

public abstract class BaseScreen extends ScreenAdapter {

	protected ActionPanel actionPanel;
	protected LevelController levelController;
	protected Game game;
	
	public BaseScreen(Game game) {
		this.game = game;
		levelController = new LevelController(game);
		actionPanel = new ActionPanel(levelController);
	}
}
