package com.xeranas.gdx.sample.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.utils.Disposable;

public class BoxModel implements Disposable {

	private ModelInstance boxInstance;
	private Model box;

	public BoxModel(Color color) {
		ModelBuilder modelBuilder = new ModelBuilder();
		Material material = new Material(ColorAttribute.createDiffuse(color));
		box = modelBuilder.createBox(1f, 1f, 1f, material, Usage.Position | Usage.Normal);
		boxInstance = new ModelInstance(box, 0, 0, 0);
	}

	public BoxModel() {
		this(Color.BLUE);
	}
	
	@Override
	public void dispose() {
		box.dispose();
	}

	public ModelInstance getBoxInstance() {
		return boxInstance;
	}
	
}
